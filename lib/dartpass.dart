import 'dart:math';
import 'package:dartpass/constdicts.dart' as lists;
import 'package:dartpass/dictionary.dart' as dmod;
import 'package:dartpass/extensions.dart';

/// Data structure used for holding password generation options
class ProgramOptions {
  var useChars = false;
  var noNumbers = false;
  var noSymbols = false;

  var dict = dmod.Dictionary(lists.dictEmpty);
  var passCount = 5;
  var size = 4;

  var capitalizeWords = true;
  var easySymbols = false;
  var min = 0;
  var max = 0;
  var excludeChars = "";

  var postOps = <PostProcOp>[];

  void addPostOp(ProcessorFunc op, dynamic extra) {
    postOps.insert(postOps.length, PostProcOp(op, extra));
  }

  /// This method sets up the dictionary for the ProgramOptions instance, including any filtering
  /// needed.
  void setupDictionary() {
    late Set<String> tempSet;

    if (useChars) {
      if (noNumbers) {
        if (noSymbols) {
          tempSet = lists.dictLetters.toSet();
        } else {
          tempSet = easySymbols
              ? lists.dictLetEasySym.toSet()
              : lists.dictLetSym.toSet();
        }
      } else {
        if (noSymbols) {
          tempSet = lists.dictLetNum.toSet();
        } else {
          tempSet = easySymbols
              ? lists.dictLetNumEasySym.toSet()
              : lists.dictLetNumSym.toSet();
        }
      }
    } else {
      tempSet = lists.dictEFF.toSet();
    }

    if (min > 0) {
      tempSet = dmod.filterMinLength(tempSet, min);
    }
    if (max > 0) {
      tempSet = dmod.filterMaxLength(tempSet, max);
    }

    if (excludeChars.isNotEmpty) {
      tempSet = dmod.filterChars(tempSet, excludeChars);
    }

    dict = dmod.Dictionary(tempSet.toList());
  }
}

/// The ProcessorFunc type is a signature used for post-processing passphrases.
///
/// This processor function is expected to take a list of tokens and modify it
/// in some way, whether modifying the tokens or the list itself. [extra] is
/// an optional parameter for passing additional data to a processor function
/// at call time.
typedef ProcessorFunc = void Function(List<String> tokens, dynamic extra);

/// A data structure to hold post-processing information
class PostProcOp {
  late ProcessorFunc op;
  dynamic extra;

  PostProcOp(this.op, this.extra);
}

/// Represents a passphrase.
///
/// Most models represent something like this as a
/// single string of characters, but we are looking at it as a series of
/// n-length tokens.
///
/// A series of tokens are added to the passphrase and additional processing
/// may be done on it, such as capitalization, adding a separator character
/// between each one, etc. This additional processing is non-destructive and
/// can be removed at any time while retaining the original tokens.
class Passphrase {
  final _tokens = <String>[];
  final _processed = <String>[];

  @override
  String toString() {
    if (_processed.isNotEmpty) {
      return _processed.join();
    }
    return _tokens.join();
  }

  /// Empties the passphrase.
  void clear() {
    _tokens.clear();
    _processed.clear();
  }

  /// Adds a separator to the passphrase at the specified index.
  ///
  /// This does not clear any post-processing because it does not change the
  /// passphrase's tokens.
  void insertSeparator(int index, String token) =>
      _processed.insert(_processed.length, token);

  /// Adds a token to the passphrase at the specified index and clears all
  /// postprocessing.
  void insertToken(int index, String token) {
    _tokens.insert(_tokens.length, token);
    _processed.clear();
  }

  /// Apply a postprocessing function to the tokens in the passphrase.
  void process(ProcessorFunc pf, dynamic extra) {
    if (_processed.isEmpty) {
      _processed.addAll(_tokens);
    }
    pf(_processed, extra);
  }

  /// Adds a token to the end of the passphrase and clears all postprocessing.
  void pushToken(String token) {
    _tokens.insert(_tokens.length, token);
    _processed.clear();
  }

  /// Deletes a separator from the passphrase at the specified index.
  ///
  /// This does not clear any post-processing because it does not change the
  /// passphrase's tokens.
  void removeSeparatorAt(int index) => _processed.removeAt(_tokens.length);

  /// Removes all post-processing from the passphrase.
  ///
  /// This is not to be confused with clear(), which empties the passphrase.
  void reset() {
    _tokens.clear();
    _processed.clear();
    _processed.addAll(_tokens);
  }
}

/// Postprocessing function which capitalizes the first letter of any word
void capitalizeWords(List<String> tokens, dynamic extra) {
  for (var i = 0; i < tokens.length; i++) {
    if (tokens[i].length > 1) {
      tokens[i] = tokens[i].toCapitalized();
    }
  }
}

enum SeparatorType {
  constantLetter,
  constantNumber,
  constantSymbol,
  constantEasySym,
  constantLetterNumber,
  constantAnyEasy,
  constantAny,
  randomLetter,
  randomNumber,
  randomSymbol,
  randomEasySym,
  randomLetterNumber,
  randomAnyEasy,
  randomAny,
  specified,
  none,
}

class Separator {
  SeparatorType sepType = SeparatorType.none;
  String? separator;

  Separator(this.sepType, this.separator);
}

/// Postprocessing function which adds another token between token boundaries.
///
/// [extra] is expected to be a Separator object. If null, this function will
/// do nothing. If given an object other than Separator, it will throw a
/// a FormatException.
void insertSeparator(List<String> tokens, dynamic extra) {
  if (extra == null) {
    return;
  }

  final sepInfo = extra as Separator;
  late String sep;
  switch (sepInfo.sepType) {
    case SeparatorType.constantLetter:
    case SeparatorType.randomLetter:
      final sepDict = dmod.Dictionary(lists.dictLetters);
      sep = sepDict.choose(1)[0];
      break;
    case SeparatorType.constantNumber:
    case SeparatorType.randomNumber:
      final sepDict = dmod.Dictionary(lists.dictNumbers);
      sep = sepDict.choose(1)[0];
      break;
    case SeparatorType.constantSymbol:
    case SeparatorType.randomSymbol:
      final sepDict = dmod.Dictionary(lists.dictSymbols);
      sep = sepDict.choose(1)[0];
      break;
    case SeparatorType.constantEasySym:
    case SeparatorType.randomEasySym:
      final sepDict = dmod.Dictionary(lists.dictEasySymbols);
      sep = sepDict.choose(1)[0];
      break;
    case SeparatorType.constantLetterNumber:
    case SeparatorType.randomLetterNumber:
      final sepDict = dmod.Dictionary(lists.dictLetNum);
      sep = sepDict.choose(1)[0];
      break;
    case SeparatorType.constantAnyEasy:
    case SeparatorType.randomAnyEasy:
      final sepDict = dmod.Dictionary(lists.dictLetNumEasySym);
      sep = sepDict.choose(1)[0];
      break;
    case SeparatorType.constantAny:
    case SeparatorType.randomAny:
      final sepDict = dmod.Dictionary(lists.dictLetNumSym);
      sep = sepDict.choose(1)[0];
      break;
    case SeparatorType.specified:
      if (sepInfo.separator == null) {
        return;
      }
      sep = sepInfo.separator ?? "";
      break;
    // This case is just to shut the linter up. We've already handled
    // SeparatorType.none.
    default:
      return;
  }

  tokens.insert(Random.secure().nextInt(tokens.length), sep);
}

/// Returns a basic diceware passphrase.
///
/// [dictionary] is a list of passphrase components -- words, letters, symbols,
/// etc. -- and [count] is the number of passphrase components to use. In this
/// way you can generate a Diceware passphrase, a regular character-based
/// complex passphrase, or something else.
Passphrase generatePassphrase(final ProgramOptions opt) {
  var out = Passphrase();

  for (var t in opt.dict.choose(opt.size)) {
    out.pushToken(t);
  }

  for (var i = 0; i < opt.postOps.length; i++) {
    out.process(opt.postOps[i].op, opt.postOps[i].extra);
  }
  return out;
}
