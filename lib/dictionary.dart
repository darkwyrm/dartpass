import 'dart:collection';
import 'dart:math';

enum BaseList {
  letters,
  numbers,
  symbols,
  easySymbols,
  letNum,
  letNumSym,
  letNumEasySym,
  eff,
}

/// The Dictionary class is a set of strings enhanced for token processing in
/// the context of passphrase generation.
class Dictionary {
  var _tokens = SplayTreeSet<String>();

  Dictionary(final List<String>? dict) {
    if (dict != null) {
      _tokens.addAll(dict);
    }
  }

  /// Adds [dict] to the dictionary, evenly spreading out the added
  /// words throughout.
  void add(final Set<String> dict) => _tokens.addAll(dict);

  /// Removes all words in [dict] from the object.
  /// Returns a random token from the dictionary using a cryptographically
  /// secure random number generator
  List<String> choose(int count) {
    final tokenList = _tokens.toList();
    var out = <String>[];

    for (var i = 0; i < count; i++) {
      out.insert(
          out.length, tokenList[Random.secure().nextInt(tokenList.length)]);
    }
    return out;
  }

  void exclude(final Set<String> dict) =>
      _tokens = SplayTreeSet<String>.from(_tokens.difference(dict));

  /// Apply a filter function to the dictionary
  void filter(DictFilterFunc df, dynamic extra) {
    var filtered = df(_tokens.toList(), extra);
    _tokens.clear();
    _tokens.addAll(filtered);
  }

  get length => _tokens.length;
}

typedef DictFilterFunc = SplayTreeSet<String> Function(
    Iterable<String> tokens, dynamic extra);

/// A data structure to hold dictionary filtering information
class DictFilterOp {
  late DictFilterFunc op;
  dynamic extra;

  DictFilterOp(this.op, this.extra);
}

/// Returns all words at least the minimum length integer supplied in [extra].
///
/// If given a null or value less than 1, this function returns the same list
/// it was given.
SplayTreeSet<String> filterMinLength(Iterable<String> tokens, dynamic extra) {
  if (extra == null) {
    return SplayTreeSet.from(tokens);
  }

  var out = SplayTreeSet<String>();
  final minLength = extra as int;
  if (minLength < 1) {
    return SplayTreeSet.from(tokens);
  }
  out.addAll(tokens.where((token) => token.length >= minLength));

  return out;
}

/// Returns all words no greater than the maximum length integer supplied in [extra].
///
/// If given a null or value less than 1, this function returns the same list
/// it was given.
SplayTreeSet<String> filterMaxLength(Iterable<String> tokens, dynamic extra) {
  if (extra == null) {
    return SplayTreeSet.from(tokens);
  }

  var out = SplayTreeSet<String>();
  final maxLength = extra as int;
  if (maxLength < 1) {
    return SplayTreeSet.from(tokens);
  }

  out.addAll(tokens.where((token) => token.length <= maxLength));

  return out;
}

/// Returns all words which do not have the character(s) supplied in [extra].
///
/// If given an empty string or null, this function returns an empty list.
SplayTreeSet<String> filterChars(Iterable<String> tokens, dynamic extra) {
  if (extra == null) {
    return SplayTreeSet.from(tokens);
  }

  var out = SplayTreeSet<String>();
  final charString = extra as String;
  if (charString == "") {
    return SplayTreeSet.from(tokens);
  }

  var temp = <String>[];
  temp.addAll(tokens);
  for (final char in charString.split("")) {
    temp = temp.where((element) => !element.contains(char)).toList();
  }

  out.addAll(temp);

  return out;
}
