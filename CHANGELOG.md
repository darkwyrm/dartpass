## 1.0.1

- Patches to separator manipulation code

## 1.0.0

- Initial release.
