import 'dart:io';
import 'package:args/args.dart';
import 'package:dartpass/dartpass.dart' as dartpass;

const gVersion = "1.0.0";

/// Turns command-line arguments into program state
dartpass.ProgramOptions handleCmdArgs(List<String> args) {
  var parser = ArgParser();

  parser.addFlag("help",
      abbr: 'h',
      defaultsTo: false,
      negatable: false,
      help: "Displays this message");

  parser.addFlag("version",
      defaultsTo: false,
      negatable: false,
      help: "Displays version information");

  parser.addOption("count",
      abbr: 'c',
      valueHelp: "n",
      help: "Number of passphrases to generate. Defaults to 5.");

  parser.addOption("size",
      abbr: 's',
      valueHelp: "n",
      help: "Number of words or characters in the passphrase. Defaults to 4 "
          "words or 8 characters.");

  parser.addFlag("easysymbols",
      abbr: 'e',
      defaultsTo: false,
      negatable: false,
      help: "Exclude symbols which often confuse non-technical users: braces, "
          "brackets, backslash, backtick, and the pipe symbol.");

  parser.addOption("exclude",
      abbr: 'x',
      valueHelp: "charstring",
      help: "A string of other characters to exclude from passphrases.");

  parser.addFlag("chars",
      defaultsTo: false,
      negatable: false,
      help:
          "Use individual characters instead of words for passphrase generation.");

  parser.addFlag("nosymbols",
      negatable: false,
      help: "Don't use symbols in character-based passphrases");

  parser.addFlag("nonumbers",
      negatable: false,
      help: "Don't use numbers in character-based passphrases");

  parser.addFlag("nocapitalize",
      negatable: false,
      help:
          "Don't capitalize the first letter of each word in word-based passphrases.");

  parser.addOption("min",
      valueHelp: "n",
      help: "Set minimum word length in word-based passphrases.");
  parser.addOption("max",
      valueHelp: "n",
      help: "Set maximum word length in word-based passphrases.");

  parser.addOption("addnumber",
      valueHelp: "n", help: "Add n numbers to word-based passphrases.");

  parser.addOption("addsymbol",
      valueHelp: "n", help: "Add n symbols to word-based passphrases.");

  late ArgResults opts;
  try {
    opts = parser.parse(args);
  } on FormatException {
    print(parser.usage);
    exit(1);
  }

  if (opts["help"]) {
    print(parser.usage);
    exit(0);
  }

  if (opts["version"]) {
    print(gVersion);
    exit(0);
  }

  var out = dartpass.ProgramOptions();

  out.useChars = opts["chars"];

  out.noSymbols = opts["nosymbols"];
  out.noNumbers = opts["nonumbers"];
  if (!out.useChars) {
    if (out.noNumbers) {
      print("--nonumbers may be used with character-based passphrases only.");
      exit(1);
    }

    if (out.noSymbols) {
      print("--nosymbols may be used with character-based passphrases only.");
      exit(1);
    }
  }

  if (opts["size"] == null) {
    out.size = opts["chars"] ? 8 : 4;
  } else {
    try {
      out.size = int.parse(opts["size"]);
    } on FormatException {
      print("size must be a number.");
      exit(1);
    }
  }

  if (opts["count"] != null) {
    try {
      out.passCount = int.parse(opts["count"]);
    } on FormatException {
      print("count must be a number.");
      exit(1);
    }
  }

  out.easySymbols = opts["easysymbols"];

  if (opts["exclude"] != null) {
    out.excludeChars = opts["exclude"];
  }

  if (opts["min"] != null) {
    try {
      out.min = int.parse(opts["min"]);
    } on FormatException {
      print("min must be a number.");
      exit(1);
    }
  }

  if (opts["max"] != null) {
    try {
      out.max = int.parse(opts["max"]);
    } on FormatException {
      print("max must be a number.");
      exit(1);
    }
  }

  if (opts["nocapitalize"] == false) {
    out.addPostOp(dartpass.capitalizeWords, null);
  }

  if (opts["addnumber"] != null) {
    try {
      final numCount = int.parse(opts["addnumber"]);
      for (var i = 0; i < numCount; i++) {
        out.addPostOp(dartpass.insertSeparator,
            dartpass.Separator(dartpass.SeparatorType.randomNumber, null));
      }
    } on FormatException {
      print("addnumber value must be a number.");
      exit(1);
    }
  }

  if (opts["addsymbol"] != null) {
    try {
      final numCount = int.parse(opts["addsymbol"]);
      for (var i = 0; i < numCount; i++) {
        if (opts["easysymbols"]) {
          out.addPostOp(dartpass.insertSeparator,
              dartpass.Separator(dartpass.SeparatorType.randomEasySym, null));
        } else {
          out.addPostOp(dartpass.insertSeparator,
              dartpass.Separator(dartpass.SeparatorType.randomSymbol, null));
        }
      }
    } on FormatException {
      print("addsymbol value must be a number.");
      exit(1);
    }
  }
  return out;
}

void main(List<String> args) {
  var opt = handleCmdArgs(args);
  opt.setupDictionary();

  for (var i = 0; i < opt.passCount; i++) {
    print(dartpass.generatePassphrase(opt));
  }
}
